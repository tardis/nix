pkgs: let
  inherit (pkgs) lib;
in {
  consts = {
    rootPubKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCi+VFpBKlawtaEOrsF2beIDz837HVIMQU0Jbs7ATbc7DGSOiuA7lzif8PpAFfuVhxqncy4asPGI/UQdng7YH2yLlhVdWy0yJ0Keld8kuqMR39BpGMuSLw5N569cdMRK01GI9s3xNwboERxEfTfDYMsMqJVI1eMeW1LySdhdfDBktn9QsH/G1UFbvg0vBBqcjADUvNIWzmbpzT5IuqRcXp5sx3+XAkbx+wmOdnTrxdPTqLtulmCaay95b3Ta4lcTfQ4HcH5t5upCnoXLe0zAbGyT2towVthD78Lh33PRrBOFi1uTmS3vejTizvHL62IbMsHTXmSZDkYxrr7hqKKlyOIihF2XhIBYYROri5QnyJoFSB87GpYFb43QpX23QQOw/m7IseDTvWO4JVC86FisrLFtzwS6BJBwe6wvc7sqWhaU/bPTi2iTvo3YlUH7fn2QBMdsLrk2DuU65+6ASGjciwSndRHYZgfe68yCR7AXm/xt6AScNbu0lY1uIT+N5mOoXk= sysmans@tardisproject.uk";

    caCert = ''      -----BEGIN CERTIFICATE-----
      MIIBlzCCAT2gAwIBAgIQEcmEsApMPiF6dPsDH6kTBTAKBggqhkjOPQQDAjAqMQ8w
      DQYDVQQKEwZUQVJESVMxFzAVBgNVBAMTDlRBUkRJUyBSb290IENBMB4XDTIyMDky
      MzEyNTQ0N1oXDTMyMDkyMDEyNTQ0N1owKjEPMA0GA1UEChMGVEFSRElTMRcwFQYD
      VQQDEw5UQVJESVMgUm9vdCBDQTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABK8C
      gt7UMC38QlJkUP0kMBKUYaAsURqEDQwWSUO+IfcMDVellwlB/x9vlVadlN7DaWFa
      YxV9pZ+Lfa+AfO57JFqjRTBDMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMBAf8ECDAG
      AQH/AgEBMB0GA1UdDgQWBBSNPkp/zPziiJMbV8ePJOw0IdcZETAKBggqhkjOPQQD
      AgNIADBFAiEAv/DpsYXAKbgpT2UGavkEqIKkmNk3p4Hp7rQQDb4CihYCIBJg1KHr
      NJUNX54vDKcfosJVtz8OwYZSvMdGmdLVlceE
      -----END CERTIFICATE-----'';

    clusterDomain = "tardisproject.uk";

    ourACME = {
      server = "https://ca.tardisproject.uk/acme/acme/directory";
    };
  };

  mkSystem = basePath:
    pkgs.inputs.nixos.lib.nixosSystem {
      inherit (pkgs) system;
      inherit pkgs;
      specialArgs = {inherit (pkgs) lib;};

      modules = [(lib.mkSystemModule basePath)];
    };

  mkSystemModule = basePath: {
    imports = [
      {nixpkgs.overlays = pkgs.overlays;}
      ./profiles/common.nix
      ./modules
      pkgs.inputs.agenix.nixosModules.default
      basePath
    ];
  };

  mkColmenaNode = name: basePath: {
    imports = [(lib.mkSystemModule basePath)];
    deployment = {
      targetHost = "${name}.internal.tardisproject.uk";
      buildOnTarget = true;
    };
  };

  recursiveMerge = with lib;
    attrList: let
      f = attrPath:
        zipAttrsWith (
          n: values:
            if tail values == []
            then head values
            else if all isList values
            then unique (concatLists values)
            else if all isAttrs values
            then f (attrPath ++ [n]) values
            else last values
        );
    in
      f [] attrList;

  systemdServiceScrapeConfig = service: extra:
    lib.recursiveMerge [
      {
        job_name = service;
        journal = {
          labels = {
            inherit service;
          };
        };
        relabel_configs = [
          {
            action = "keep";
            source_labels = ["__journal__systemd_unit"];
            regex = "${service}.*";
          }
          {
            source_labels = ["__journal__systemd_unit"];
            target_label = "systemd_unit";
            action = "replace";
          }
        ];
      }
      extra
    ];

  subdomain = sub:
    if builtins.stringLength sub > 0
    then "${sub}.${lib.consts.clusterDomain}"
    else lib.consts.clusterDomain;
}
