let
  hosts = {
    web = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+z2XEPsY8nXw/uvT1LQRIzbM4EVnOXTruzX84kOi/Xtb2i5LZzaV9uK9G415hrjY+2JlMmlOJQbd9mQ3FQN3gjxAdDr4TJRbdRXQrsbNJiXIiECH3AsEw6VGLMWab8cBdZvBSVEDaTVChjiCDtr0Oc8mnxO9XSJX+acc0R/WSEpH1qPuno936PV5uctBRWiJYWbVsnOP8n01YxemQykWLHEYpgK67JSHnvHtXVUsp1CaIFxcC2b5Akj+9m0QhbGEjl74e775Twp9z0TpTJE8iZwZPTtfw+vFYTsiLx6dBfnMcowmikCDAma80CrQj7EDvXB5IcauDemD9FcBA+laQlyqOt15dugj5O1fl2jPmS15tWyjBen2HVpwttOW9DNKi2N3WX+w8TWtcXUj5SyghQmJ2wZxHieS99zaWNrg+LT7iqjFZp967X1cQQtglR7sxyvZHgPTZJzBuKsxxl7G/bL5Pg0I26BJcVXrYbRotR74/jwKkh8DN6unYFIgFK0/idjZ/Yad9/TooCptUAMjjcZAwTn7+PE2TYdnIaOn9A5QeUJIc+LKHnQ2PfAM5toyXzGqN4/NkgAp9rjpyC7RqDV/taH/WUd3L6vHBa57756kHJJao/GNQn4ulAV3ENqN4HBFlazME9chGRBkkl880mVHT6SIHRgt5ic5w5BztZQ== root@web";
    enclave = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCsZqIreH7HQVrowLTqFO1Vlw3PV9SadeCi/d9cnvdrYFFkb7vt4Q0/6Z3wQWToqFzwnabKwxI5SH7Z7xrQ8QoC1mMkvtmQcCqSQANc2rBeQD5d38dFTyhCtRgDCD3tBbXp1/UNjlbudUA8jjVfhwv7DSF8buh+cCGckcN0b86y+RJLvzFyF+63LsVoiG8aKJ1WySCQ1PY/y9FsuZD7isOJi4uTXbLQD5WzEorEwN5wPS0frRNOyO45qv8ay5EAmASPZdcH2Xg/6YTJ/+THq4oU0pl5Fg1j8RwfhtDABGktKaKIqZClT51aYccEcPpJ9bF+FYlh6qNSvxGedLfZXtqesdDId7NhqysqLy9A+g+5IXtZz1gr8XAEKuT7l/xTYm9Gy8RvBz2uF1TsqxHJz7K5Kwop9sv5UdLfJHm2MheZU9ezf8uyBITlqOTDgwlaSQHFm/06PGtDCQiAhXTHZ4CKNdgLjKnk3SoVh2FKgJpAOyKX4ou34L22qbXNZi4kiM2FTJ5JY7e1ACCV/HjxVvieZ5yNTxdBP0fUjGkg3Mooaw9p1juRjf2FbMLEegyprnIEaTTVX8MKFRnDiVFRBPEfqlmwdtEVmCdr/oL0jx0G6tVh4rL756Txnpf4/DzEqcvzsaMFbFzhVOvrP08CbJEfdiz5ckPkM4EnukkRl/t2Sw== root@enclave";
    monitoring = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCqBL8EMFkdFjyoCdwoCh2AgMF3nmTu76IFyKOhCx3m3P4SmlMopZzJ91yCWJbskyzNZ/c8b2y9rODZsesJTWc+CEmAEJ8WPyy9QgRVYkLBlO0Gtf3w1154V493B6kO+sDiphjAeVygHFrXpV5mS3iBVFxHqVGgMYi2lck38O0G9/Brtf6cWULjARIScZjnx8yY4/8Oh489QwN4Yj0dQZ7WaXR83JFy8doDDBM2dCltA+hU8TC4oj7VkVtBs35Ty8g8uoJGXKtXAQOI4j9+T5fwcZdzclxgyoZOj+KrvJtqxadRZQ2p2gkKKzDPYvRJzO4BkwBi7HJg75EeuUPDFfx8VrxVyIuajDl5SyCWVgWa1VqUjLYI+DJDzy3EOAAJlIrcXgKgJdJR0swiihR0PNBCsh8PhTe+Dzc+u8bAv0DnAjm7uM22PKADRV8Yy76yXaKaTuBJcXs1IDGhWXsRauVxQH6uNaHZSrOO4WkOv0JfUBJgj6Ff2ktS/E0cTxRz1aoIR2OzIFDDAB1qwJ5Lbbj0X+QG/G2WY4usD6e0kGZxzi27dGZwucL7JjtnzNI6o7FGObk/aOX/zEyzXRWq4UGd//o5vncULIDo3hB39Lzy1uQuaOOuXoVy8zXSbXoU422F0BT65Q+l3Tcd53ZyhaCoMUbID8vuj6MYR9glhMw/vQ== root@monitoring";
  };
  sysmans = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCi+VFpBKlawtaEOrsF2beIDz837HVIMQU0Jbs7ATbc7DGSOiuA7lzif8PpAFfuVhxqncy4asPGI/UQdng7YH2yLlhVdWy0yJ0Keld8kuqMR39BpGMuSLw5N569cdMRK01GI9s3xNwboERxEfTfDYMsMqJVI1eMeW1LySdhdfDBktn9QsH/G1UFbvg0vBBqcjADUvNIWzmbpzT5IuqRcXp5sx3+XAkbx+wmOdnTrxdPTqLtulmCaay95b3Ta4lcTfQ4HcH5t5upCnoXLe0zAbGyT2towVthD78Lh33PRrBOFi1uTmS3vejTizvHL62IbMsHTXmSZDkYxrr7hqKKlyOIihF2XhIBYYROri5QnyJoFSB87GpYFb43QpX23QQOw/m7IseDTvWO4JVC86FisrLFtzwS6BJBwe6wvc7sqWhaU/bPTi2iTvo3YlUH7fn2QBMdsLrk2DuU65+6ASGjciwSndRHYZgfe68yCR7AXm/xt6AScNbu0lY1uIT+N5mOoXk= sysmans@tardisproject.uk";
in {
  "smallstepPassword.age".publicKeys = [hosts.enclave sysmans];
  "vaultwardenEnv.age".publicKeys = [hosts.enclave sysmans];
  "sonicRocketToml.age".publicKeys = [hosts.enclave sysmans];

  "civicrmCronSecrets.age".publicKeys = [hosts.web sysmans];
  "dokuwikiEmailUser.age".publicKeys = [hosts.web sysmans];
  "dokuwikiOauthSecret.age".publicKeys = [hosts.web sysmans];
  "dokuwikiUsersFile.age".publicKeys = [hosts.web sysmans];
  "dovecotLdapConf.age".publicKeys = [hosts.web sysmans];
  "dovecotOauthConf.age".publicKeys = [hosts.web sysmans];
  "postfixLdapConf.age".publicKeys = [hosts.web sysmans];
  "postfixLdapAliasesConf.age".publicKeys = [hosts.web sysmans];
  "flamePassword.age".publicKeys = [hosts.web sysmans];
  "oauthProxy.age".publicKeys = [hosts.web sysmans];
  "caddyEnv.age".publicKeys = [hosts.web sysmans];

  "minioMetricsBearer.age".publicKeys = [hosts.monitoring sysmans];
  "grafanaEnv.age".publicKeys = [hosts.monitoring sysmans];
  "lokiEnv.age".publicKeys = [hosts.monitoring sysmans];
  "tempoEnv.age".publicKeys = [hosts.monitoring sysmans];
  "mktxpConfig.age".publicKeys = [hosts.monitoring sysmans];
  "discordWebhook.age".publicKeys = [hosts.monitoring sysmans];
}
