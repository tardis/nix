{config, ...}: {
  imports = [
    ../profiles/libvirt_guest.nix
    ../profiles/web
    ../profiles/email
    ../profiles/dns.nix
    ../profiles/netdata/child.nix
  ];

  networking.hostName = "web";

  services.netdata.apiKey = "5332994a-ed48-4a4d-90f7-00f727793f3b";

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/e62b8fab-4782-4249-a212-38ae58594714";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/D3AE-901E";
    fsType = "vfat";
  };

  networking.useDHCP = true;
  networking.firewall.allowedTCPPorts = [22 80 443];
}
