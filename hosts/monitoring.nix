{...}: {
  imports = [
    ../profiles/libvirt_guest.nix

    ../profiles/netdata/parent.nix
    ../profiles/metrics/microtik.nix
    ../profiles/metrics/ssacli.nix
    ../profiles/metrics/uptime.nix
  ];

  networking.hostName = "monitoring";
  networking.useDHCP = true;

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/81003972-7ff9-40db-a4c8-e3495173f8c5";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/B36D-0029";
    fsType = "vfat";
  };
}
