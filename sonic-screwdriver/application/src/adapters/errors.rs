use std::{error::Error, fmt::Display};

#[derive(Debug)]
pub enum DBError {
    NotFound,
    ConstraintFailed,
    OtherError(sqlx::Error),
}
impl From<sqlx::Error> for DBError {
    fn from(value: sqlx::Error) -> Self {
        match value {
            sqlx::Error::RowNotFound => Self::NotFound,
            sqlx::Error::Database(db) if db.code().is_some_and(|c| c == "1555") => {
                Self::ConstraintFailed
            }
            e => Self::OtherError(e),
        }
    }
}

impl Error for DBError {}
impl Display for DBError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NotFound => write!(f, "Could not find entry in database"),
            Self::ConstraintFailed => write!(f, "A constraint failed to pass"),
            Self::OtherError(e) => write!(f, "{e}"),
        }
    }
}
