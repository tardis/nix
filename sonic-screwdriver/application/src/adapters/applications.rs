use anyhow::{Context, Result};
use serde::Serialize;
use sonic_types::{application::PatchApplication, Password, User};
use sqlx::{query, query_as, FromRow};

use super::{DBError, Pool};

pub struct ApplicationsAdapter {
    pool: Pool,
}

impl ApplicationsAdapter {
    pub const fn new(pool: Pool) -> Self {
        Self { pool }
    }

    pub async fn by_id(&self, id: i32) -> Result<ApplicationRow, DBError> {
        query_as::<_, ApplicationRow>("SELECT * FROM application WHERE id = $1")
            .bind(id)
            .fetch_one(&self.pool)
            .await
            .map_err(Into::into)
    }

    pub async fn accept(&self, id: i32) -> Result<()> {
        query(
            "UPDATE application SET accepted = TRUE, decision_made_on = datetime('now') WHERE id = $1",
        )
        .bind(id)
            .execute(&self.pool).await?;

        Ok(())
    }

    pub async fn deny(&self, id: i32) -> Result<()> {
        query(
            "UPDATE application SET accepted = FALSE, decision_made_on = datetime('now') WHERE id = $1",
        )
        .bind(id)
            .execute(&self.pool).await?;

        Ok(())
    }

    pub async fn pending(&self) -> Result<Vec<ApplicationRow>> {
        query_as::<_, ApplicationRow>(
            "SELECT * FROM application WHERE accepted IS NULL AND email_verify_token IS NULL",
        )
        .fetch_all(&self.pool)
        .await
        .map_err(Into::into)
    }

    pub async fn update(&self, id: i32, app: PatchApplication) -> Result<()> {
        query(
            "UPDATE application SET
                first_name = $2,
                last_name = $3,
                email = $4,
                preferred_username = $5,
                graduation_year = $6,
                is_legacy_user = $7,
                comments = $8
            WHERE id = $1",
        )
        .bind(id)
        .bind(&*app.first_name)
        .bind(&*app.last_name)
        .bind(app.email.to_string())
        .bind(&*app.preferred_username)
        .bind(app.graduation_year)
        .bind(app.is_legacy_user)
        .bind(app.comments.as_deref())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn insert(&self, app: &PatchApplication, verify_token: &Password) -> Result<()> {
        query(
            "INSERT INTO application
                (first_name, last_name, email, preferred_username, graduation_year, is_legacy_user, comments, email_verify_token)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
        )
        .bind(&*app.first_name)
        .bind(&*app.last_name)
        .bind(app.email.to_string())
        .bind(&*app.preferred_username)
        .bind(app.graduation_year)
        .bind(app.is_legacy_user)
        .bind(app.comments.as_deref())
        .bind(verify_token.secret())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn verify(&self, token: &str) -> Result<bool> {
        let result = query(
            "UPDATE application
                SET email_verify_token = NULL
                WHERE email_verify_token = $1",
        )
        .bind(token)
        .execute(&self.pool)
        .await?;

        Ok(result.rows_affected() > 0)
    }
}

#[derive(Serialize, FromRow, Debug, Clone)]
pub struct ApplicationRow {
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub preferred_username: String,
    pub graduation_year: Option<i32>,
    pub is_legacy_user: bool,
    pub comments: Option<String>,
    pub accepted: Option<bool>,
    pub decision_made_on: Option<String>,
}

impl ApplicationRow {
    pub fn try_into_user(self, sponsor: Option<String>) -> Result<User> {
        Ok(User::Person {
            username: self
                .preferred_username
                .try_into()
                .context("Username is invalid")?,
            first_name: self.first_name,
            last_name: self.last_name,

            external_email: self.email.parse().context("External email is invalid")?,
            graduation_year: self.graduation_year,
            is_legacy_user: self.is_legacy_user,
            sponsor: sponsor.filter(|x| !x.is_empty()),
        })
    }
}
