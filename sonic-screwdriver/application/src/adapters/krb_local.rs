use std::process::Stdio;

use crate::config::Kerberos as KerberosConfig;
use anyhow::{anyhow, Context, Result};
use sonic_types::{Password, Username};
use tokio::{io::AsyncWriteExt, process::Command};

pub struct LocalKerberosAdapter {
    config: KerberosConfig,
}

impl LocalKerberosAdapter {
    pub fn new(config: &KerberosConfig) -> Self {
        Self {
            config: config.clone(),
        }
    }

    pub async fn add_principal(&self, username: &Username, password: &Password) -> Result<()> {
        self.execute_kadmin(
            &format!("ank {username}"),
            &format!("{}\n{}\n", password.secret(), password.secret()),
        )
        .await
    }

    pub async fn reset_password(&self, uid: &str, new_password: &Password) -> Result<()> {
        self.execute_kadmin(
            &format!("cpw {uid}"),
            &format!("{}\n{}\n", new_password.secret(), new_password.secret()),
        )
        .await
    }

    pub async fn change_password_using_old(
        &self,
        username: &Username,
        old_password: &Password,
        new_password: &Password,
    ) -> Result<()> {
        // spawn kpasswd
        let mut child = Command::new("kpasswd")
            .arg(&(**username))
            .stdout(Stdio::piped())
            .stdin(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .context("error executing kadmin")?;

        // write to child stdin
        child
            .stdin
            .as_mut()
            .unwrap()
            .write_all(
                &format!(
                    "{}\n{}\n{}\n",
                    old_password.secret(),
                    new_password.secret(),
                    new_password.secret()
                )
                .into_bytes(),
            )
            .await
            .context("error writing to command stdin")?;

        // deal with response
        let resp = child
            .wait_with_output()
            .await
            .context("error waiting for kpasswd to finish")?;
        if resp.status.success() {
            Ok(())
        } else {
            Err(anyhow!(
                "kadmin returned code {}. \nstdout:\n {}\nstderr:\n {}",
                resp.status.code().unwrap(),
                std::str::from_utf8(&resp.stdout).unwrap(),
                std::str::from_utf8(&resp.stderr).unwrap()
            ))
        }
    }

    async fn execute_kadmin(&self, query: &str, inp: &str) -> Result<()> {
        let mut child = Command::new("kadmin")
            .arg("-r")
            .arg(&self.config.realm)
            .arg("-p")
            .arg(&self.config.principal)
            .arg("-q")
            .arg(query)
            .stdout(Stdio::inherit())
            .stdin(Stdio::piped())
            .stderr(Stdio::inherit())
            .spawn()
            .context("error executing kadmin")?;

        child
            .stdin
            .as_mut()
            .unwrap()
            .write_all(&format!("{}\n{}", self.config.password, inp).into_bytes())
            .await
            .context("error writing admin password + input to command stdin")?;

        let resp = child
            .wait()
            .await
            .context("error waiting for kadmin to finish")?;

        if !resp.success() {
            return Err(anyhow!("kadmin returned code {}", resp.code().unwrap()));
        }

        Ok(())
    }
}
