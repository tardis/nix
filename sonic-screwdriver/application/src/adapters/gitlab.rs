use anyhow::Result;
use chrono::{Duration, Utc};
use jsonwebtoken::{Algorithm, EncodingKey, Header};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use sonic_types::gitlab::GitlabPage;

use crate::config::Gitlab as GitlabConfig;

pub struct GitlabAdapter {
    conf: GitlabConfig,
    client: Client,
}

impl GitlabAdapter {
    pub fn new(conf: &GitlabConfig) -> Self {
        Self {
            client: Client::new(),
            conf: conf.clone(),
        }
    }

    /// Checks if the GitLab token is expired by sending a request to `/api/v4/users` and analysing
    /// the response.
    ///
    /// # Errors
    ///
    /// Returns an error if something unexpected happened when trying to get a response from the
    /// above URL. Otherwise, will return a [`bool`], true if the token is expired, false if it's
    /// not.
    async fn is_token_expired(&self) -> Result<bool, reqwest::Error> {
        let response: String = self
            .client
            .get(format!("{}/api/v4/users", self.conf.url))
            .bearer_auth(&self.conf.api_key)
            .send()
            .await?
            .text()
            .await?;

        // Determine this by doing some string checking. This should be fast *enough*, though
        // GitLab will probably change its output format eventually.
        // WARNING TO FUTURE MAINTAINER: The expected output for an invalid token, at this moment
        // in time is this:
        // {"error":"invalid_token","error_description":"Token is expired. You can either do re-authorization or token refresh."}
        // If something to do with GitLab mysteriously fails, maybe the token is expried or GitLab
        // have changed the format of their error JSON.
        Ok(response.contains("invalid_token") && response.contains("Token is expired."))
    }

    /// Get all usernames of active users on gitlab
    pub async fn get_usernames(&self) -> Result<Vec<String>> {
        if self.is_token_expired().await? {
            anyhow::bail!(
                "The GitLab token has expired! \
                Contact a Tardis GitLab admin to generate a new personal access token."
            );
        }

        let mut page = 0;
        let mut usernames = vec![];
        loop {
            let resp: Vec<UserAPIRow> = self
                .client
                .get(format!(
                    "{}/api/v4/users?exclude_internal=true&active=true&page={}",
                    self.conf.url, page
                ))
                .bearer_auth(&self.conf.api_key)
                .send()
                .await?
                .json()
                .await?;

            if resp.is_empty() {
                break;
            }

            usernames.extend(resp.into_iter().map(|r| r.username));
            page += 1;
        }

        Ok(usernames)
    }

    /// Get gitlab pages endpoints for a given domain.
    /// This uses an internal API, which is pretty bad, however there's no other way unfortunately.
    pub async fn get_pages(&self, domain: String) -> Result<Vec<GitlabPage>> {
        // Encode a JWT using the internal secret
        let key = EncodingKey::from_base64_secret(&self.conf.internal_secret)?;
        let exp = Utc::now() + Duration::hours(1);
        let jwt = jsonwebtoken::encode(
            &Header::new(Algorithm::HS256),
            &JWTClaims {
                iss: "gitlab-pages",
                iat: Utc::now().timestamp(),
                exp: exp.timestamp(),
            },
            &key,
        )?;

        let resp: InternalPagesResp = self
            .client
            .get(format!(
                "{}/api/v4/internal/pages?host={}",
                self.conf.url, domain,
            ))
            .header("Gitlab-Pages-Api-Request", jwt)
            .send()
            .await?
            .json()
            .await?;

        Ok(resp
            .lookup_paths
            .into_iter()
            .map(|p| GitlabPage {
                domain: domain.clone(),
                prefix: p.prefix,
            })
            .collect())
    }
}

#[derive(Deserialize)]
struct UserAPIRow {
    username: String,
}

#[derive(Serialize)]
struct JWTClaims {
    iss: &'static str,
    iat: i64,
    exp: i64,
}

#[derive(Deserialize)]
struct InternalPagesResp {
    lookup_paths: Vec<LookupPath>,
}

#[derive(Deserialize)]
struct LookupPath {
    prefix: String,
}

#[cfg(test)]
mod tests {
    use crate::config::Config;

    use super::*;

    /// Try to load the Sonic config. If it can't be loaded, temporarily ignore the test.
    fn get_gitlab_config() -> anyhow::Result<Config> {
        use anyhow::Context;
        // This is taken from the http/main.rs. We should investiage making this a shared function
        // rather than copy-pasting here.
        let config =
            std::env::var("SONIC_HTTP_CONFIG").unwrap_or_else(|_| "./Sonic.toml".to_string());
        let config = std::fs::read(config.as_str()).context(format!(
            "error reading Sonic.toml. Tried to access the path {config}"
        ))?;
        let config = String::from_utf8(config).context("error decoding Sonic.toml as utf-8")?;

        toml::from_str(&config).context("The toml crate failed to parse Sonic.toml")
    }

    #[tokio::test]
    async fn parse_gitlab_usernames() {
        // If we can't get the config, just pass the test.
        let Ok(config) = get_gitlab_config() else {
            println!("GitLab config generation failed.");
            return;
        };
        let gitlab_adapter = GitlabAdapter::new(&config.gitlab);
        let usernames = gitlab_adapter.get_usernames().await.unwrap();
        // We should have more than 2 active GitLab users.
        assert!(usernames.len() > 2);
        println!("{usernames:?}");
    }
}
