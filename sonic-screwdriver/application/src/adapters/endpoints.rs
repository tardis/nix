use std::{net::Ipv4Addr, str::FromStr};

use anyhow::Result;
use futures_util::{StreamExt, TryStreamExt};
use sqlx::{query, query_as, FromRow};

use super::{DBError, Pool};
use sonic_types::{
    endpoint::{Domain, Endpoint, InternalIpAddr, NewEndpoint},
    Username,
};

pub struct EndpointsAdapter {
    pool: Pool,
}

impl EndpointsAdapter {
    pub const fn new(pool: Pool) -> Self {
        Self { pool }
    }

    pub async fn get_by_user(&self, id: &str) -> Result<Vec<Endpoint>> {
        query_as::<_, EndpointRow>(
            "SELECT e.prefix, e.strip_prefix, e.target_ip, e.target_port, e.target_is_https, e.force_https, e.host_override, d.id AS domain_id, d.domain, d.owner
            FROM endpoint AS e, domain AS d
            WHERE d.owner = $1 AND e.domain_id = d.id",
        )
        .bind(id)
        .fetch(&self.pool)
            .map(|row| Endpoint::try_from(row?))
            .try_collect().await
    }

    pub async fn add_endpoint(&self, uid: &str, endpoint: &NewEndpoint) -> Result<(), DBError> {
        query(
            "INSERT INTO endpoint
            (prefix, domain_id, target_ip, target_port, target_is_https, strip_prefix, force_https, host_override)
            VALUES (
                $1,
                (SELECT id FROM domain WHERE domain = $2 AND owner = $3),
                $4, $5, $6, $7, $8, $9
            )",
        )
        .bind(&*endpoint.prefix)
        .bind(&endpoint.domain)
        .bind(uid)
        .bind(endpoint.target_ip.to_string())
        .bind(endpoint.target_port)
        .bind(endpoint.target_is_https)
        .bind(endpoint.strip_prefix)
        .bind(endpoint.force_https)
        .bind(endpoint.host_override.clone())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn get_all(&self) -> Result<Vec<Endpoint>> {
        query_as::<_, EndpointRow>(
           "SELECT e.prefix, e.strip_prefix, e.target_ip, e.target_port, e.target_is_https, e.force_https, e.host_override, d.id AS domain_id, d.domain, d.owner
            FROM endpoint AS e, domain AS d
            WHERE d.id = e.domain_id",
        )
        .fetch(&self.pool)
            .map(|row| Endpoint::try_from(row?))
            .try_collect().await
    }

    pub async fn delete_with_user(&self, uid: &Username, prefix: &str, domain: &str) -> Result<()> {
        query(
            "DELETE
            FROM endpoint
            WHERE ROWID IN (
                SELECT e.ROWID
                FROM endpoint AS e, domain AS d
                WHERE e.prefix = $1 AND d.domain = $2 AND d.owner = $3 AND d.id = e.domain_id
            )",
        )
        .bind(prefix)
        .bind(domain)
        .bind(&**uid)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn get_with_user(
        &self,
        uid: &Username,
        prefix: &str,
        domain: &str,
    ) -> Result<Option<Endpoint>> {
        query_as::<_, EndpointRow>(
            "SELECT e.prefix, e.strip_prefix, e.target_ip, e.target_port, e.target_is_https, e.force_https, e.host_override, d.id AS domain_id, d.domain, d.owner
            FROM endpoint AS e, domain AS d
            WHERE e.prefix = $1 AND d.domain = $2 AND d.owner = $3 AND d.id = e.domain_id
            LIMIT 1", // We shouldn't need this as it should be unique but just incase
        ).bind(prefix).bind(domain).bind(&**uid).fetch_optional(&self.pool).await?.map(Endpoint::try_from).transpose()
    }
}

#[derive(Debug, FromRow)]
pub struct EndpointRow {
    prefix: String,
    target_ip: String,
    target_port: u16,
    target_is_https: bool,
    force_https: bool,
    strip_prefix: bool,
    host_override: Option<String>,

    domain_id: i32,
    domain: String,
    owner: String,
}

impl TryFrom<EndpointRow> for Endpoint {
    type Error = anyhow::Error;

    fn try_from(r: EndpointRow) -> Result<Self, Self::Error> {
        Ok(Self {
            domain: Domain {
                id: r.domain_id,
                owner: r.owner.try_into()?,
                domain: r.domain,
            },
            prefix: r.prefix.into(),
            strip_prefix: r.strip_prefix,
            target_ip: InternalIpAddr::new(Ipv4Addr::from_str(&r.target_ip)?)?,
            target_port: r.target_port,
            target_is_https: r.target_is_https,
            force_https: r.force_https,
            host_override: r.host_override,
        })
    }
}
