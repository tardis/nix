use crate::App;
use anyhow::Result;
use sonic_types::Username;
use std::collections::HashMap;

use super::errors::HideErrorExt;

impl App {
    pub async fn create_namespace(&self, username: &Username) -> Result<()> {
        let mut labels = HashMap::new();
        labels.insert("tardisproject.uk/owner", &**username);

        self.k8s
            .create_namespace(username, labels)
            .await
            .hide_err("Error creating namespace")?;

        Ok(())
    }
    pub async fn remove_namespace(&self, username: &Username) -> Result<()> {
        self.k8s
            .delete_namespace(username)
            .await
            .hide_err("Error deleting namespace")
    }

    pub async fn namespace_exists(&self, username: &Username) -> Result<bool> {
        self.k8s
            .namespace_exists(username)
            .await
            .hide_err("Error checking namespace exists")
    }
}
