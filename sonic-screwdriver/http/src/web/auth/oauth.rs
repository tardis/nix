use axum::{
    extract::{Query, State},
    http::StatusCode,
    response::{IntoResponse, Redirect},
};
use axum_extra::extract::{
    cookie::{Cookie, SameSite},
    CookieJar,
};
use oauth2::{
    basic::{
        BasicErrorResponse, BasicRequestTokenError, BasicRevocationErrorResponse,
        BasicTokenIntrospectionResponse, BasicTokenType,
    },
    reqwest::async_http_client,
    AuthorizationCode, Client, CsrfToken, PkceCodeChallenge, PkceCodeVerifier, Scope,
    StandardRevocableToken, StandardTokenResponse, TokenResponse,
};
use serde::{Deserialize, Serialize};
use std::sync::Arc;

use super::{AuthState, UserInfo};

/// A minorly adjusted [`BasicClient`] to include the extra JWT
/// sent when using `OpenID Connect` protocol provided by keycloak
pub type OAuthClient = Client<
    BasicErrorResponse,
    StandardTokenResponse<OpenIdConnectExtraTokenFields, BasicTokenType>,
    BasicTokenType,
    BasicTokenIntrospectionResponse,
    StandardRevocableToken,
    BasicRevocationErrorResponse,
>;

pub async fn login(
    cookies: CookieJar,
    user: Option<UserInfo>,
    State(auth): State<Arc<AuthState>>,
) -> impl IntoResponse {
    if user.is_some() {
        return (cookies, Redirect::to("/"));
    }

    // Generate a challenge and csrf token
    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();
    let (auth_url, csrf_token) = auth
        .client
        .authorize_url(CsrfToken::new_random)
        // Needed so that keycloak sends us an id_token
        .add_scope(Scope::new("openid".to_string()))
        .set_pkce_challenge(pkce_challenge)
        .url();

    // Store them so we can verify later
    let cookies = cookies
        .add(
            Cookie::new("csrf_token", csrf_token.secret().clone()), // .same_site(SameSite::Lax)
                                                                    // .finish(),
        )
        .add(
            Cookie::new("pkce_verifier", pkce_verifier.secret().clone()), // .same_site(SameSite::Lax)
                                                                          // .finish(),
        );

    // // Redirect to auth url
    (cookies, Redirect::to(auth_url.as_str()))
}

/// Contains any extra fields returned during token exchange that we wish to deserialise
#[derive(Deserialize, Serialize, Debug)]
pub struct OpenIdConnectExtraTokenFields {
    /// ID Token, stored as a JWT
    /// [See OpenID Connect Specification](https://openid.net/specs/openid-connect-core-1_0.html#IDToken)
    id_token: String,
}

impl oauth2::ExtraTokenFields for OpenIdConnectExtraTokenFields {}

#[derive(Deserialize)]
pub struct OAuthCallback {
    state: String,
    code: String,
}

pub async fn auth(
    cookies: CookieJar,
    State(auth): State<Arc<AuthState>>,
    Query(cb): Query<OAuthCallback>,
) -> Result<impl IntoResponse, AuthCallbackError> {
    // Get challenge and csrf token verifier
    let pkce_verifier = PkceCodeVerifier::new(
        cookies
            .get("pkce_verifier")
            .ok_or(AuthCallbackError::MissingCookies)?
            .value()
            .to_string(),
    );
    let csrf_token = cookies
        .get("csrf_token")
        .ok_or(AuthCallbackError::MissingCookies)?;

    // Verify csrf token
    if csrf_token.value() != cb.state {
        return Err(AuthCallbackError::MissingCookies)?;
    }

    // Avoid using them twice
    let cookies = cookies
        .remove(Cookie::named("pkce_verifier"))
        .remove(Cookie::named("csrf_token"));

    // Do rest of token verification and exchange
    let token_result = auth
        .client
        .exchange_code(AuthorizationCode::new(cb.code))
        .set_pkce_verifier(pkce_verifier)
        .request_async(async_http_client)
        .await
        .map_err(AuthCallbackError::RequestError)?;

    // Store the tokens
    let max_age = token_result
        .expires_in()
        .ok_or(AuthCallbackError::InvalidJWT)?;
    let max_age: time::Duration = max_age.try_into().unwrap();

    let mut cookies: CookieJar = cookies.add(
        Cookie::build("jwt", token_result.access_token().secret().to_owned())
            .max_age(max_age)
            .same_site(SameSite::Lax)
            .finish(),
    );

    // Check that the id_token is valid
    if auth
        .jwks
        .validate(&token_result.extra_fields().id_token)
        .is_some()
    {
        cookies = cookies.add(
            Cookie::build("id_token", token_result.extra_fields().id_token.clone())
                .max_age(max_age)
                .same_site(SameSite::Lax)
                .finish(),
        );
    }

    Ok((cookies, Redirect::to("/")))
}

/// Represents an error returned by the auth callback
#[derive(Debug)]
pub enum AuthCallbackError {
    MissingCookies,
    #[expect(dead_code, reason = "This is used in the Debug implementation.")]
    RequestError(BasicRequestTokenError<oauth2::reqwest::Error<reqwest::Error>>),
    InvalidJWT,
}

impl IntoResponse for AuthCallbackError {
    fn into_response(self) -> axum::response::Response {
        dbg!(self);
        (
            StatusCode::FORBIDDEN,
            "Error authenticating. Please try again.",
        )
            .into_response()
    }
}
