use axum::routing::get;
use oauth2::{url, AuthUrl, ClientId, ClientSecret, RedirectUrl, TokenUrl};
use reqwest::Url;
use sonic_application::config::Config;

use crate::state::Router;

use self::{jwt::JwtValidator, oauth::OAuthClient};

mod jwt;
mod oauth;

pub use jwt::{UserInfo, UserIsAdmin};

pub struct AuthState {
    client: OAuthClient,
    end_session_uri: String,
    login_uri: String,
    jwks: JwtValidator,
}

impl AuthState {
    /// Generates the uri to which we redirect the user to perform a logout
    ///
    /// View what the query parameters mean
    /// [here](https://openid.net/specs/openid-connect-rpinitiated-1_0.html#RPLogout)
    pub fn generate_end_session_uri(&self, id_token: &str) -> Result<String, url::ParseError> {
        let uri = Url::parse_with_params(
            &self.end_session_uri,
            &[
                ("client_id", self.client.client_id().as_str()),
                ("post_logout_redirect_uri", self.login_uri.as_str()),
                ("id_token_hint", id_token),
            ],
        )?;

        Ok(uri.to_string())
    }
}

pub async fn state(config: &Config) -> AuthState {
    // Extract oauth config
    let client = OAuthClient::new(
        ClientId::new(config.auth.client_id.clone()),
        Some(ClientSecret::new(config.auth.client_secret.clone())),
        AuthUrl::new(config.auth.auth_uri.clone()).unwrap(),
        Some(TokenUrl::new(config.auth.token_uri.clone()).unwrap()),
    )
    .set_redirect_uri(RedirectUrl::new(config.auth.redirect_uri.clone()).unwrap());
    let jwks = JwtValidator::new(config).await.unwrap();

    AuthState {
        client,
        end_session_uri: config.auth.end_session_uri.clone(),
        jwks,
        login_uri: config.auth.login_uri.clone(),
    }
}

pub fn routes() -> Router {
    Router::new()
        .route("/login", get(oauth::login))
        .route("/auth", get(oauth::auth))
}
