use std::sync::Arc;

use axum::{
    extract::State,
    response::{IntoResponse, Redirect},
    routing::{get, post},
};
use axum_flash::{Flash, IncomingFlashes};
use sonic_application::App;

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, utils::ErrorResponses,
};

use super::auth::UserInfo;

pub fn routes() -> Router {
    Router::new()
        .route("/", get(info))
        .route("/create", post(create))
        .route("/delete", post(remove))
}

pub async fn info(
    State(app): State<Arc<App>>,
    user: UserInfo,
    flash: IncomingFlashes,
) -> impl IntoResponse {
    let exists = app.namespace_exists(&user.uid).await.unwrap_or_else(|e| {
        eprintln!("Error checking namespace existence for {}: {}", user.uid, e);
        false
    });

    let templ = render(
        "k8s-info.html",
        &context! {
            username: &user.uid,
            exists: exists,
            msgs: flash.for_template()
        },
    );

    (flash, templ)
}

pub async fn create(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    app.create_namespace(&user.uid)
        .await
        .err_to_redirect(&flash, "/k8s")?;

    Ok((
        flash.success("Created your namespace successfully! Now you can login."),
        Redirect::to("/k8s"),
    ))
}

pub async fn remove(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    app.remove_namespace(&user.uid)
        .await
        .err_to_redirect(&flash, "/k8s")?;

    Ok((
        flash.success("Removed your namespace and all contents. It will take some time for everything to spin down, so don't create a new one for a few minutes."),
        Redirect::to("/"),
    ))
}
