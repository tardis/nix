#![warn(clippy::all, clippy::pedantic, clippy::nursery)]
#![allow(clippy::module_name_repetitions, clippy::missing_errors_doc)]

use std::{env, fs, net::SocketAddr, sync::Arc};

use anyhow::{Context, Result};
use axum_flash::Key;
use sonic_application::{config::Config, App};
use tower_http::services::fs::ServeDir;
use tracing::{debug, info};

mod api;
mod flash;
mod state;
mod template;
mod traefik;
pub mod utils;
mod web;

pub(crate) use state::Router;

use crate::state::AppState;

#[tokio::main]
async fn main() {
    init_log();

    let config = load_config().expect("error loading config");
    debug!(event = "loaded config", config = ?config);

    let addr = SocketAddr::new(config.http.host.parse().unwrap(), config.http.port);

    let static_dir =
        env::var("SONIC_STATIC_DIR").unwrap_or_else(|_| config.http.static_dir.clone());

    let serve_static = ServeDir::new(&static_dir);
    let auth = web::auth::state(&config).await;

    template::configure(&config);

    let app = App::new(&config, template::TERA.get().unwrap().clone())
        .await
        .expect("error creating app");

    let router = Router::new()
        .merge(web::routes())
        .nest("/api", api::routes());

    let router: axum::Router<()> =
        router
            .nest_service("/public", serve_static)
            .with_state(AppState {
                app: Arc::new(app),
                config: Arc::new(config),
                auth: Arc::new(auth),
                flash: axum_flash::Config::new(Key::generate()),
                captcha: Arc::default(),
            });

    info!(event = "started listening", addr = %addr);
    axum::Server::bind(&addr)
        .serve(router.into_make_service())
        .await
        .unwrap();
}

fn init_log() {
    tracing_subscriber::fmt::init();
}

fn load_config() -> Result<Config> {
    let config = env::var("SONIC_HTTP_CONFIG").unwrap_or_else(|_| "./Sonic.toml".to_string());
    let config = fs::read(config.as_str()).context(format!(
        "error reading Sonic.toml. Tried to access the path {config}"
    ))?;
    let config = String::from_utf8(config).context("error decoding Sonic.toml as utf-8")?;

    toml::from_str(&config).context("error parsing Sonic.toml")
}
