use std::sync::Arc;

use axum::extract::FromRef;
use sonic_application::{config::Config, App};

use crate::web::{auth::AuthState, captcha::CaptchaStore};

pub type Router = axum::Router<AppState>;

#[derive(Clone, FromRef)]
pub struct AppState {
    pub app: Arc<App>,
    pub config: Arc<Config>,
    pub auth: Arc<AuthState>,
    pub flash: axum_flash::Config,
    pub captcha: Arc<CaptchaStore>,
}
