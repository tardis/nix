use std::fmt::Display;

use axum::{
    http::StatusCode,
    response::{IntoResponse, Redirect},
    Json,
};
use axum_flash::Flash;
use serde::{ser::SerializeStruct, Serialize};

/// Extension for Result<T, E> that allows us to turn errors into redirects with a flash message.
pub trait ErrorResponses<R> {
    fn err_to_redirect(self, flash: &Flash, path: &str) -> Result<R, (Flash, Redirect)>;
    fn err_to_json(self, status: StatusCode) -> Result<R, JSONError>;
}

impl<R, E: Display> ErrorResponses<R> for Result<R, E> {
    // TODO: This involves cloning, which i'm not super happy about but is necessary to make this ergonomic
    fn err_to_redirect(self, flash: &Flash, path: &str) -> Result<R, (Flash, Redirect)> {
        self.map_err(|e| (flash.clone().error(format!("{e:#}")), Redirect::to(path)))
    }

    fn err_to_json(self, status: StatusCode) -> Result<R, JSONError> {
        self.map_err(|e| JSONError {
            message: format!("{e:#}"),
            status,
        })
    }
}

#[derive(Debug)]
pub struct JSONError {
    status: StatusCode,
    message: String,
}

impl Serialize for JSONError {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut obj = serializer.serialize_struct("JSONError", 2)?;
        obj.serialize_field("success", &false)?;
        obj.serialize_field("message", &self.message)?;
        obj.end()
    }
}

impl IntoResponse for JSONError {
    fn into_response(self) -> axum::response::Response {
        (self.status, Json(self)).into_response()
    }
}
