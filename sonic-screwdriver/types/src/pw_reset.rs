use serde::Deserialize;

#[derive(Deserialize)]
pub struct ResetPwRequest {
    pub email: String,
}

#[derive(Deserialize)]
pub struct PerformResetPwRequest {
    pub password: String,
    pub confirm_password: String,
}
