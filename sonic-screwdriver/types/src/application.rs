use lettre::Address as EmailAddress;

use super::{Username, VarLengthString};

#[derive(Clone, Debug)]
pub struct PatchApplication {
    pub first_name: VarLengthString<1, 50>,
    pub last_name: VarLengthString<1, 50>,
    pub email: EmailAddress,
    pub preferred_username: Username,
    pub graduation_year: Option<i32>,
    pub is_legacy_user: bool,
    pub comments: Option<VarLengthString<0, 500>>,
}
