use serde::{Deserialize, Deserializer};

pub fn fallible_deser<'de, D, T: Default + Deserialize<'de>>(de: D) -> Result<T, D::Error>
where
    D: Deserializer<'de>,
{
    match T::deserialize(de) {
        Ok(x) => Ok(x),
        Err(_) => Ok(Default::default()),
    }
}

pub fn deser_checkbox_default() -> bool {
    false
}
