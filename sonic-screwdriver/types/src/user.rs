use lettre::Address as EmailAddress;

use super::Username;

pub enum User {
    System {
        username: Username,
    },
    Person {
        username: Username,
        first_name: String,
        last_name: String,

        external_email: EmailAddress,
        graduation_year: Option<i32>,
        is_legacy_user: bool,
        sponsor: Option<String>,
    },
}

impl User {
    pub fn username(&self) -> &Username {
        match self {
            Self::System { username } | Self::Person { username, .. } => username,
        }
    }
}
