#[derive(Debug)]
pub struct GitlabPage {
    pub domain: String,
    pub prefix: String,
}
