use std::str::FromStr;

use anyhow::Context;
use serde::{Deserialize, Serialize};

use crate::endpoint::{Domain, Endpoint, InternalIpAddr, NewEndpoint};

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateEndpointRequest {
    pub prefix: String,
    pub domain: String,
    pub target_ip: String,
    pub target_port: u16,
    #[serde(default = "crate::utils::deser_checkbox_default")]
    pub target_is_https: bool,

    #[serde(default = "crate::utils::deser_checkbox_default")]
    pub strip_prefix: bool,

    #[serde(default = "crate::utils::deser_checkbox_default")]
    pub force_https: bool,

    pub host_override: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct APICreateEndpointRequest {
    pub prefix: String,
    pub domain: String,
    pub target_ip: String,
    pub target_port: u16,
    pub target_is_https: bool,
    pub strip_prefix: bool,
    pub force_https: bool,
    pub host_override: Option<String>,
}

impl TryInto<NewEndpoint> for CreateEndpointRequest {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<NewEndpoint, Self::Error> {
        Ok(NewEndpoint {
            domain: self.domain,
            prefix: self.prefix.into(),
            target_ip: InternalIpAddr::from_str(&self.target_ip)
                .context("Error parsing target IP")?,
            target_port: self.target_port,
            target_is_https: self.target_is_https,
            strip_prefix: self.strip_prefix,
            force_https: self.force_https,
            host_override: self
                .host_override
                .map(|x| x.trim().to_string())
                .filter(|x| !x.is_empty()),
        })
    }
}

impl TryInto<NewEndpoint> for APICreateEndpointRequest {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<NewEndpoint, Self::Error> {
        Ok(NewEndpoint {
            domain: self.domain,
            prefix: self.prefix.into(),
            strip_prefix: self.strip_prefix,
            target_ip: InternalIpAddr::from_str(&self.target_ip)
                .context("Error parsing target IP")?,
            target_port: self.target_port,
            target_is_https: self.target_is_https,
            force_https: self.force_https,
            host_override: self
                .host_override
                .map(|x| x.trim().to_string())
                .filter(|x| !x.is_empty()),
        })
    }
}

#[derive(Serialize, Deserialize)]
pub struct EndpointRequest {
    pub prefix: String,
    pub domain: String,
}

#[derive(Serialize, Deserialize)]
pub struct ListEndpointsResponse {
    pub endpoints: Vec<Endpoint>,
    pub domains: Vec<Domain>,
}
