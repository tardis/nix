use anyhow::{anyhow, Context};
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::application::PatchApplication;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ApplyRequest {
    pub first_name: String,
    pub last_name: String,
    pub preferred_username: String,
    pub email: String,

    #[serde(deserialize_with = "crate::utils::fallible_deser")]
    pub graduation_year: Option<i32>,

    #[serde(default = "crate::utils::deser_checkbox_default")]
    pub is_legacy_user: bool,

    pub comments: Option<String>,

    pub captcha_answer: Option<String>,
}

impl TryFrom<ApplyRequest> for PatchApplication {
    type Error = anyhow::Error;

    fn try_from(value: ApplyRequest) -> Result<Self, Self::Error> {
        let curr_year = OffsetDateTime::now_utc().year();
        if value
            .graduation_year
            .map(|year| year < curr_year)
            .unwrap_or(false)
        {
            return Err(anyhow!("Graduation year must be in future or this year."));
        }

        let comments = match value.comments {
            Some(x) => Some(x.try_into().context("Comments are invalid")?),
            None => None,
        };
        Ok(PatchApplication {
            first_name: value
                .first_name
                .try_into()
                .context("First name is not valid")?,
            last_name: value
                .last_name
                .try_into()
                .context("Last name is not valid")?,
            email: value.email.parse().context("Email is not valid")?,
            preferred_username: value
                .preferred_username
                .try_into()
                .context("Username is not valid")?,
            graduation_year: value.graduation_year,
            is_legacy_user: value.is_legacy_user,
            comments,
        })
    }
}
