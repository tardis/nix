use std::str::FromStr;

use anyhow::{bail, Context, Result};
use clap::Subcommand;
use dialoguer::{Confirm, Input, Select};
use sonic_types::endpoint::{Domain, InternalIpAddr, PathPrefix};
use sonic_types::web::endpoints::{
    CreateEndpointRequest, DeleteEndpointRequest, ListEndpointsResponse,
};

use crate::http::Client;
use log::*;

#[derive(Debug, Subcommand)]
pub enum Commands {
    #[command(visible_alias = "ls")]
    /// List your current endpoints
    List,

    #[command(visible_alias = "c", alias = "new", alias = "n")]
    /// Create a new endpoint
    Create {
        #[arg(short, long)]
        domain: Option<String>,

        #[arg(short, long)]
        prefix: Option<String>,

        #[arg(short, long)]
        target: Option<String>,

        #[arg(long)]
        https: Option<bool>,

        #[arg(long)]
        strip_prefix: Option<bool>,
    },

    /// Delete an endpoint
    Delete {
        #[arg(short, long)]
        domain: Option<String>,

        #[arg(short, long)]
        prefix: Option<String>,
    },
}

pub fn dispatch(cmd: Commands) -> Result<()> {
    match cmd {
        Commands::List => list(),
        Commands::Create {
            domain,
            target,
            prefix,
            https,
            strip_prefix,
        } => create(domain, prefix, target, https, strip_prefix),
        Commands::Delete { domain, prefix } => delete(domain, prefix),
    }
}

fn list() -> Result<()> {
    let client = Client::new_authenticated().context("Error creating HTTP client")?;

    let ListEndpointsResponse { domains, endpoints } = client
        .list_endpoints()
        .context("Error fetching user endpoints")?;

    info!("Available domains ({}):", domains.len());
    for domain in domains.iter() {
        println!("{}", domain.domain());
    }

    println!();
    info!("Endpoints ({})", endpoints.len());
    for endpoint in endpoints.iter() {
        print!("{} - to {}", endpoint.link(), endpoint.dest_display());
        if endpoint.strip_prefix {
            print!(" (prefix stripped)");
        }
        println!()
    }

    Ok(())
}

fn create(
    domain: Option<String>,
    prefix: Option<String>,
    target: Option<String>,
    https: Option<bool>,
    strip_prefix: Option<bool>,
) -> Result<()> {
    let client = Client::new_authenticated().context("Error creating HTTP client")?;

    let domain = if let Some(x) = domain {
        x
    } else {
        let ListEndpointsResponse { domains, .. } = client
            .list_endpoints()
            .context("Error fetching user domains")?;

        let items: Vec<_> = domains.iter().map(Domain::domain).collect();
        let selection = Select::new()
            .with_prompt("Domain")
            .items(&items)
            .interact()?;

        items[selection].to_string()
    };

    let prefix: PathPrefix = if let Some(x) = prefix {
        x
    } else {
        Input::new()
            .with_prompt("Path Prefix")
            .with_initial_text("/")
            .interact_text()?
    }
    .into();

    let target_ip: InternalIpAddr = if let Some(target) = target.as_ref() {
        let ip = target.split(':').next().unwrap_or("");
        InternalIpAddr::from_str(ip).context("Invalid target IP address")?
    } else {
        Input::new()
            .with_prompt("Target IP Address")
            .interact_text()?
    };

    let target_port: u16 = if let Some(target) = target {
        let ip = target.split(':').last().unwrap_or("");
        u16::from_str(ip).context("Invalid target port")?
    } else {
        Input::new().with_prompt("Target Port").interact_text()?
    };

    let https = if let Some(https) = https {
        https
    } else {
        Confirm::new()
            .with_prompt("Use HTTPS to endpoint")
            .default(false)
            .interact()?
    };

    let strip_prefix = if let Some(strip_prefix) = strip_prefix {
        strip_prefix
    } else {
        Confirm::new()
            .with_prompt("Strip the prefix before it gets to the destination?")
            .default(false)
            .interact()?
    };

    let endpoint = client
        .create_endpoint(&CreateEndpointRequest {
            domain,
            prefix: prefix.to_string(),
            target_ip: target_ip.to_string(),
            target_port,
            target_is_https: https,
            strip_prefix,
            force_https: true,   // TODO
            host_override: None, // TODO
        })
        .context("Error creating user endpoint")?;

    info!("Created new endpoint:");

    print!("{} - to {}", endpoint.link(), endpoint.dest_display());
    if endpoint.strip_prefix {
        print!(" (prefix stripped)");
    }
    println!();

    Ok(())
}

fn delete(filter_domain: Option<String>, prefix: Option<String>) -> Result<()> {
    let client = Client::new_authenticated().context("Error creating HTTP client")?;
    let ListEndpointsResponse { mut endpoints, .. } = client
        .list_endpoints()
        .context("Error fetching user endpoints")?;

    if let Some(filter_domain) = filter_domain {
        endpoints.retain(|e| e.domain.domain() == filter_domain);
    }

    if endpoints.is_empty() {
        bail!("No endpoints to delete");
    }

    let endpoint = if let Some(prefix) = prefix {
        let Some(endpoint) = endpoints.into_iter().find(|e| *e.prefix == prefix) else {
            bail!("No endpoint with that domain and prefix found");
        };

        endpoint
    } else {
        let items: Vec<_> = endpoints.iter().map(|e| e.link()).collect();
        let selected = Select::new()
            .items(&items)
            .with_prompt("Select endpoint to delete")
            .interact()?;

        endpoints.remove(selected)
    };

    client.delete_endpoint(&DeleteEndpointRequest {
        prefix: endpoint.prefix.into_inner(),
        domain: endpoint.domain.into_domain(),
    })?;

    info!("Endpoint deleted.");

    Ok(())
}
