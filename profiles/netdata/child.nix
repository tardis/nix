{
  pkgs,
  lib,
  config,
  ...
}: {
  options.services.netdata.apiKey = lib.mkOption {
    type = lib.types.str;
  };

  config = {
    services.netdata = {
      enable = true;
      config = {
        db = {
          mode = "ram";
          retention = "120";
          "update every" = 30;
        };
        ml = {enabled = "no";};
        health = {enabled = "no";};
        web = {mode = "none";};
      };

      configDir = {
        "stream.conf" = pkgs.writeText "stream.conf" ''
          [stream]
            enabled = yes
            destination = ${lib.subdomain "monitoring.internal"}:19999
            api key = ${config.services.netdata.apiKey}
        '';

        "health_alarm_notify.conf" =
          pkgs.writeText "netdata-no-notify" ''
          '';
      };
    };

    # wait-for-netdata-up is broken on new versions
    systemd.services.netdata.serviceConfig.ExecStartPost = lib.mkForce [];
  };
}
