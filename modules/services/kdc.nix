{
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with types; let
  indent = "  ";

  mkRelation = name: value:
    if (isList value)
    then concatMapStringsSep "\n" (mkRelation name) value
    else "${name} = ${mkVal value}";

  mkVal = value:
    if (value == true)
    then "true"
    else if (value == false)
    then "false"
    else if (isInt value)
    then (toString value)
    else if (isAttrs value)
    then let
      configLines =
        concatLists
        (map (splitString "\n")
          (mapAttrsToList mkRelation value));
    in
      (concatStringsSep "\n${indent}"
        (["{"] ++ configLines))
      + "\n}"
    else value;

  mkMappedAttrsOrString = value:
    concatMapStringsSep "\n"
    (line:
      if builtins.stringLength line > 0
      then "${indent}${line}"
      else line)
    (splitString "\n"
      (
        if isAttrs value
        then
          concatStringsSep "\n"
          (mapAttrsToList mkRelation value)
        else value
      ));
in {
  options.services.kdc = {
    enable = mkOption {
      type = bool;
      default = false;
    };
    openFirewall = mkOption {
      type = bool;
      default = false;
    };
    extraConfig = mkOption {
      type = str;
      default = "";
    };
    ports = mkOption {
      type = listOf int;
      default = [88];
    };
    tcpPorts = mkOption {
      type = listOf int;
      default = [88];
    };
    realms = mkOption {
      type = either attrs lines;
      default = {};
    };
    dbDefaults = mkOption {
      type = either attrs lines;
      default = {};
    };
    dbModules = mkOption {
      type = either attrs lines;
      default = {};
    };
    kdcDefaults = mkOption {
      type = either attrs lines;
      default = {};
    };
  };

  config = mkIf config.services.kdc.enable (
    let
      kdcConfig = config.services.kdc;
      defaultRealmConfig = {
        master_key_type = "aes256-cts";
        max_life = "7d";
        max_renewable_life = "14d";
        acl_file = "/var/lib/krb5kdc/kadm5.acl";
      };
      realmConfigs = builtins.mapAttrs (_: x: defaultRealmConfig // x) config.services.kdc.realms;
    in {
      environment.etc."krb5kdc/kdc.conf".text = ''
        [kdcdefaults]
        ${mkMappedAttrsOrString kdcConfig.kdcDefaults}
         kdc_ports = ${lib.concatStringsSep "," (map builtins.toString config.services.kdc.ports)}
         kdc_tcp_ports = ${lib.concatStringsSep "," (map builtins.toString config.services.kdc.tcpPorts)}

        [realms]
        ${mkMappedAttrsOrString realmConfigs}

        [dbdefaults]
        ${mkMappedAttrsOrString kdcConfig.dbDefaults}

        [dbmodules]
        ${mkMappedAttrsOrString kdcConfig.dbModules}

        [logging]
            default = STDERR
            kdc = FILE:/var/log/krb5kdc.log
            admin_server = FILE:/var/log/kadmind.log
        ${config.services.kdc.extraConfig}
      '';

      networking.firewall = mkIf config.services.kdc.openFirewall {
        allowedTCPPorts = [88 749 464];
        allowedUDPPorts = [88 749 464];
      };

      systemd.services.krb5-kdc = {
        description = "Kerberos 5 KDC";
        path = [pkgs.krb5];
        script = "krb5kdc -n -w 2";
        wantedBy = ["multi-user.target"];
      };

      systemd.services.krb5-kadmin = {
        description = "Kerberos 5 kadmin";
        path = [pkgs.krb5];
        script = "kadmind -nofork";
        wantedBy = ["multi-user.target"];
      };
    }
  );
}
