{
  pkgs,
  lib,
  config,
  ...
}: {
  options.services.netdata.prometheusScrapers = lib.mkOption {
    type = with lib.types; listOf str;
    default = [];
  };

  config = lib.mkIf config.services.netdata.enable {
    services.netdata.configDir."go.d/prometheus.conf" = pkgs.writeText "netdata-prometheus.conf" ''
      jobs:
      ${lib.concatStrings config.services.netdata.prometheusScrapers}
    '';
  };
}
